package fileUtils

import (
	"os"
	"path/filepath"
	"runtime"
	"strings"
)

var (
	_, b, _, _ = runtime.Caller(0)
	Root       = filepath.Join(filepath.Dir(b), "../../..")
)

func ReadLines(name string) []string {
	path := Root + "/files/" + name
	return strings.Split(ReadFile(path), "\n")
}

func ReadFile(name string) string {
	file, err := os.ReadFile(name)
	checkError(err)
	return string(file)
}

func checkError(err error) {
	if err != nil {
		panic("¿Què fas?")
	}
}
