package numberUtils

import (
	"math"
	"strconv"
)

// Calcula el mínimo positivo comparando ambos parámetros recibidos. Descarta cualquier número inferior a 0
func GetMinPositive(value1 int, value2 int) (int, bool) {
	result := math.MaxInt
	if value1 >= 0 {
		result = min(result, value1)
	}
	if value2 >= 0 {
		result = min(result, value2)
	}
	if result == math.MaxInt {
		return -1, false
	} else {
		return result, true
	}
}

// Calcula el máximo positivo comparando ambos parámetros recibidos. Descarta cualquier número inferior a 0
func GetMaxPositive(value1 int, value2 int) (int, bool) {
	result := -1
	if value1 >= 0 {
		result = max(result, value1)
	}
	if value2 >= 0 {
		result = max(result, value2)
	}
	if result == -1 {
		return -1, false
	} else {
		return result, true
	}
}

func ToInt(value string) int {
	intValue, _ := strconv.Atoi(value)
	return intValue
}

func IntPow(num1 int, num2 int) int {
	return int(math.Pow(float64(num1), float64(num2)))
}

func GetLCM(numbers []int) int {
	maxNumber := GetMaxNumberInArray(numbers)
	index := 1
	for {
		currentMaxNumberMultiple := maxNumber * index
		if NumberIsLCM(numbers, currentMaxNumberMultiple) {
			return currentMaxNumberMultiple
		}
		index++
	}
}

func NumberIsLCM(numbers []int, numberThatCanBeLCM int) bool {
	for _, number := range numbers {
		if numberThatCanBeLCM%number != 0 {
			return false
		}
	}
	return true
}

func GetMaxNumberInArray(numbers []int) int {
	maxNumber := math.MinInt32
	for _, number := range numbers {
		maxNumber = max(maxNumber, number)
	}
	return maxNumber
}
