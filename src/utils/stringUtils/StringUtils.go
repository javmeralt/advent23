package stringUtils

import (
	"strings"
)

func RemoveRedundantSpaces(input string) string {
	return strings.Join(strings.Fields(input), " ")
}

func SplitTrim(input string, splitChar string) []string {
	stringWithoutRedundantSpaces := RemoveRedundantSpaces(input)
	splitStrings := strings.Split(stringWithoutRedundantSpaces, splitChar)
	for i := range splitStrings {
		splitStrings[i] = strings.TrimSpace(splitStrings[i])
	}
	return splitStrings
}

func SliceRuneFromStartToEnd(input []rune, start int, end int) string {
	return string(input[start:end])
}

func SliceRuneFromStart(input []rune, start int) string {
	return string(input[start:])
}
