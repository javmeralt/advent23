package collectionUtils

import (
	"math"
)

func GetMapKeys[M ~map[K]V, K comparable, V any](dst M) []K {
	keys := make([]K, 0, len(dst))
	for k := range dst {
		keys = append(keys, k)
	}
	return keys
}

func GetMapValues[M ~map[K]V, K comparable, V any](dst M) []V {
	values := make([]V, 0, len(dst))
	for _, v := range dst {
		values = append(values, v)
	}
	return values
}

func PutAll[M ~map[K]V, K comparable, V any](dst M, src M) {
	for k, v := range src {
		dst[k] = v
	}
}

func MinIntegerInArray(array []int) int {
	minElement := math.MaxInt32
	for _, element := range array {
		minElement = min(minElement, element)
	}
	return minElement
}

func GroupBy[K comparable, V any](array []V, keyExtractor func(V) K) map[K][]V {
	groupByMap := map[K][]V{}
	for _, value := range array {
		key := keyExtractor(value)
		listOfValuesWithSameKey := groupByMap[key]
		listOfValuesWithSameKey = append(listOfValuesWithSameKey, value)
	}
	return groupByMap
}
