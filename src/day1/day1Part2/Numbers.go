package day1Part2

import (
	"advent23/src/utils/numberUtils"
	"math"
	"strings"
)

var (
	numbers = make(map[string]string)
)

func init() {
	numbers["1"] = "one"
	numbers["2"] = "two"
	numbers["3"] = "three"
	numbers["4"] = "four"
	numbers["5"] = "five"
	numbers["6"] = "six"
	numbers["7"] = "seven"
	numbers["8"] = "eight"
	numbers["9"] = "nine"
}

func SearchLeftAndRightNumberInText(text string) (string, string) {
	leftNumber := SearchLeftNumber(text)
	rightNumber := SearchRightNumber(text)
	return leftNumber, rightNumber
}

func SearchLeftNumber(text string) string {
	minIndex := math.MaxInt
	var valueToReturn string
	for numberAsInteger, numberAsWord := range numbers {
		index, isPresent := numberIsPresentFromTheLeft(text, numberAsInteger, numberAsWord)
		if isPresent && index < minIndex {
			minIndex = index
			valueToReturn = numberAsInteger
		}
	}
	return valueToReturn
}
func SearchRightNumber(text string) string {
	maxIndex := -1
	var valueToReturn string
	for numberAsInteger, numberAsWord := range numbers {
		index, isPresent := numberIsPresentFromTheRight(text, numberAsInteger, numberAsWord)
		if isPresent && index > maxIndex {
			maxIndex = index
			valueToReturn = numberAsInteger
		}
	}
	return valueToReturn
}

func numberIsPresentFromTheLeft(text string, numberAsInteger string, numberAsWord string) (int, bool) {
	integerPosition := strings.Index(text, numberAsInteger)
	wordPosition := strings.Index(text, numberAsWord)
	return numberUtils.GetMinPositive(integerPosition, wordPosition)
}

func numberIsPresentFromTheRight(text string, numberAsInteger string, numberAsWord string) (int, bool) {
	integerPosition := strings.LastIndex(text, numberAsInteger)
	wordPosition := strings.LastIndex(text, numberAsWord)
	return numberUtils.GetMaxPositive(integerPosition, wordPosition)
}
