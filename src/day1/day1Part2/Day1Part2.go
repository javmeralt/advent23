package day1Part2

import (
	"advent23/src/utils/fileUtils"
	"strconv"
)

func PrintSolution(file string) {
	lines := fileUtils.ReadLines(file)
	total := 0
	for _, line := range lines {
		valueOfLine := getValueOfLine(line)
		total += valueOfLine
	}
	print(total)
}

func getValueOfLine(line string) int {
	leftValue, rightValue := SearchLeftAndRightNumberInText(line)
	lineValue, _ := strconv.Atoi(leftValue + rightValue)
	return lineValue
}
