package day1Part1

import (
	"advent23/src/utils/fileUtils"
	"strconv"
	"unicode"
)

func PrintSolution(file string) {
	lines := fileUtils.ReadLines(file)
	total := 0
	for _, line := range lines {
		valueOfLine := getValueOfLine(line)
		total += valueOfLine
	}
	print(total)
}

func getValueOfLine(line string) int {
	lineAsChars := []rune(line)
	leftInteger := getLeftInteger(lineAsChars)
	rigthInteger := getRightInteger(lineAsChars)
	intValue, _ := strconv.Atoi(leftInteger + rigthInteger)
	return intValue
}

func getLeftInteger(line []rune) string {
	for _, value := range line {
		if unicode.IsDigit(value) {
			return string(value)
		}
	}
	return ""
}

func getRightInteger(line []rune) string {
	for i := len(line) - 1; i >= 0; i-- {
		value := line[i]
		if unicode.IsDigit(value) {
			return string(value)
		}
	}
	return ""
}
