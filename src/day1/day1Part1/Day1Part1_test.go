package day1Part1

import "testing"

func TestPrintResult(t *testing.T) {
	PrintSolution("day1/day1.txt")
}

func TestPrintResultDemo(t *testing.T) {
	PrintSolution("day1/day1Part1Demo.txt")
}
