package day8Part1

import (
	"advent23/src/utils/fileUtils"
	"strings"
)

var (
	nodes = make(map[string]*Node)
)

func PrintSolution(file string) {
	lines := fileUtils.ReadLines(file)
	instructions := []rune(lines[0])
	loadNodes(lines[2:])
	totalMovements := getNumberOfMovements(instructions)
	print(totalMovements)
}

func getNumberOfMovements(instructions []rune) int {
	totalMovements := 0
	currentNode := nodes["AAA"]
	for {
		movements, currentNodeIsFinalNode, nextNode := iterateInstructions(instructions, currentNode)
		currentNode = nextNode
		totalMovements += movements
		if currentNodeIsFinalNode {
			break
		}
	}
	return totalMovements
}

func iterateInstructions(instructions []rune, currentNode *Node) (int, bool, *Node) {
	movements := 0
	for _, instruction := range instructions {
		if currentNode.name == "ZZZ" {
			return movements, true, currentNode
		} else {
			currentNode = getNextNode(instruction, currentNode)
			movements++
		}
	}
	return movements, currentNode.name == "ZZZ", currentNode
}

func getNextNode(instruction rune, node *Node) *Node {
	if instruction == 'L' {
		return nodes[node.left]
	} else {
		return nodes[node.right]
	}
}

func loadNodes(nodesAsString []string) {
	for _, nodeAsString := range nodesAsString {
		indexOfEquals := strings.Index(nodeAsString, "=")
		nodeName := nodeAsString[:indexOfEquals-1]
		node := getOrCreateNode(nodeName)
		setLeftAndRight(node, nodeAsString)
	}
}

func setLeftAndRight(node *Node, nodeAsString string) {
	totalLength := len(nodeAsString)
	firstParenthesis := strings.Index(nodeAsString, "(")
	comma := strings.Index(nodeAsString, ",")
	leftNodeName := nodeAsString[firstParenthesis+1 : comma]
	rightNodeName := nodeAsString[comma+2 : totalLength-1]
	node.right = rightNodeName
	node.left = leftNodeName
}

func getOrCreateNode(nodeName string) *Node {
	node, existsNode := nodes[nodeName]
	if !existsNode {
		node = createNode(nodeName)
		nodes[node.name] = node
	}
	return node
}

func createNode(name string) *Node {
	return &Node{name: name}
}

type Node struct {
	name  string
	right string
	left  string
}
