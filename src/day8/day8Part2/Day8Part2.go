package day8Part2

import (
	"advent23/src/utils/fileUtils"
	"advent23/src/utils/numberUtils"
	"strings"
)

var (
	nodes         = make(map[string]*Node)
	startingNodes = make([]*Node, 0)
)

func PrintSolution(file string) {
	lines := fileUtils.ReadLines(file)
	instructions := []rune(lines[0])
	loadNodes(lines[2:])
	totalMovements := getNumberOfMovementsOfEveryNode(instructions)
	print(totalMovements)
}

func getNumberOfMovementsOfEveryNode(instructions []rune) int {
	var movements []int
	for _, startingNode := range startingNodes {
		movements = append(movements, getNumberOfMovements(instructions, startingNode))
	}
	return numberUtils.GetLCM(movements)
}

func getNumberOfMovements(instructions []rune, currentNode *Node) int {
	totalMovements := 0
	for {
		movements, currentNodeIsFinalNode, nextNode := iterateInstructions(instructions, currentNode)
		currentNode = nextNode
		totalMovements += movements
		if currentNodeIsFinalNode {
			break
		}
	}
	return totalMovements
}

func iterateInstructions(instructions []rune, currentNode *Node) (int, bool, *Node) {
	movements := 0
	for _, instruction := range instructions {
		if isEndingNode(currentNode) {
			return movements, true, currentNode
		} else {
			currentNode = getNextNode(instruction, currentNode)
			movements++
		}
	}
	return movements, isEndingNode(currentNode), currentNode
}

func getNextNode(instruction rune, node *Node) *Node {
	if instruction == 'L' {
		return nodes[node.left]
	} else {
		return nodes[node.right]
	}
}

func loadNodes(nodesAsString []string) {
	for _, nodeAsString := range nodesAsString {
		indexOfEquals := strings.Index(nodeAsString, "=")
		nodeName := nodeAsString[:indexOfEquals-1]
		node := getOrCreateNode(nodeName)
		if isStartingNode(nodeName) {
			startingNodes = append(startingNodes, node)
		}
		setLeftAndRight(node, nodeAsString)
	}
}

func isStartingNode(nodeName string) bool {
	return strings.HasSuffix(nodeName, "A")
}

func isEndingNode(node *Node) bool {
	return strings.HasSuffix(node.name, "Z")
}

func setLeftAndRight(node *Node, nodeAsString string) {
	totalLength := len(nodeAsString)
	firstParenthesis := strings.Index(nodeAsString, "(")
	comma := strings.Index(nodeAsString, ",")
	leftNodeName := nodeAsString[firstParenthesis+1 : comma]
	rightNodeName := nodeAsString[comma+2 : totalLength-1]
	node.right = rightNodeName
	node.left = leftNodeName
}

func getOrCreateNode(nodeName string) *Node {
	node, existsNode := nodes[nodeName]
	if !existsNode {
		node = createNode(nodeName)
		nodes[node.name] = node
	}
	return node
}

func createNode(name string) *Node {
	return &Node{name: name}
}

type Node struct {
	name  string
	right string
	left  string
}
