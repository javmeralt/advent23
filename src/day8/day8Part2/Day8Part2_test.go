package day8Part2

import "testing"

func TestPrintSolution(t *testing.T) {
	PrintSolution("day8/day8.txt")
}

func TestPrintSolutionDemo(t *testing.T) {
	PrintSolution("day8/day8Demo.txt")
}
