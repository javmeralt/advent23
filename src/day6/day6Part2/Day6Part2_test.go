package day6Part2

import "testing"

func TestPrintSolution(t *testing.T) {
	PrintSolution("day6/day6.txt")
}

func TestPrintSolutionDemo(t *testing.T) {
	PrintSolution("day6/day6Demo.txt")
}
