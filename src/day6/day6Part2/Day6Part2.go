package day6Part2

import (
	"advent23/src/utils/fileUtils"
	"advent23/src/utils/numberUtils"
	"strings"
)

func PrintSolution(file string) {
	lines := fileUtils.ReadLines(file)
	race := createRace(lines)
	waysToBeatARace := calculateWaysToBeatARace(race)
	print(waysToBeatARace)
}

func createRace(lines []string) Race {
	time := getIntValue(lines[0])
	distance := getIntValue(lines[1])
	return Race{distanceToTraverse: distance, availableTime: time}
}

func getIntValue(line string) int {
	doubleColonPosition := strings.Index(line, ":")
	lineWithoutHeader := line[doubleColonPosition+1:]
	value := strings.ReplaceAll(lineWithoutHeader, " ", "")
	return numberUtils.ToInt(value)
}

func calculateWaysToBeatARace(race Race) int {
	total := 0
	for timePressingTheButton := 0; timePressingTheButton <= race.availableTime; timePressingTheButton++ {
		timeToRun := race.availableTime - timePressingTheButton
		distance := timePressingTheButton * timeToRun
		if distance > race.distanceToTraverse {
			total++
		} else if total > 0 {
			break
		}
	}
	return total
}

type Race struct {
	availableTime      int
	distanceToTraverse int
}
