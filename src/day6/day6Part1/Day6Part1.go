package day6Part1

import (
	"advent23/src/utils/fileUtils"
	"advent23/src/utils/numberUtils"
	"advent23/src/utils/stringUtils"
	"strings"
)

func PrintSolution(file string) {
	lines := fileUtils.ReadLines(file)
	races := createRaces(lines)
	waysToBeatEveryRace := calculateWaysToBeatEveryRace(races)
	print(multiplyArray(waysToBeatEveryRace))
}

func createRaces(lines []string) []Race {
	var races []Race
	times := getValuesAsIntArray(lines[0])
	distances := getValuesAsIntArray(lines[1])
	for i := 0; i < len(times); i++ {
		race := Race{distanceToTraverse: distances[i], timeAvailable: times[i]}
		races = append(races, race)
	}
	return races
}

func getValuesAsIntArray(line string) []int {
	doubleColonPosition := strings.Index(line, ":")
	lineWithoutHeader := line[doubleColonPosition+1:]
	var valuesArray []int
	values := stringUtils.SplitTrim(lineWithoutHeader, " ")
	for _, valueAsString := range values {
		valueAsInt := numberUtils.ToInt(valueAsString)
		valuesArray = append(valuesArray, valueAsInt)
	}
	return valuesArray
}

func calculateWaysToBeatEveryRace(races []Race) []int {
	var waysToBeatEveryRace []int
	for _, race := range races {
		waysToBeatARace := calculateWaysToBeatARace(race)
		waysToBeatEveryRace = append(waysToBeatEveryRace, waysToBeatARace)
	}
	return waysToBeatEveryRace
}

func calculateWaysToBeatARace(race Race) int {
	total := 0
	for timePressingTheButton := 0; timePressingTheButton <= race.timeAvailable; timePressingTheButton++ {
		timeToRun := race.timeAvailable - timePressingTheButton
		distance := timePressingTheButton * timeToRun
		if distance > race.distanceToTraverse {
			total++
		} else if total > 0 {
			break
		}
	}
	return total
}

func multiplyArray(waysToBeatEveryRace []int) int {
	result := 1
	for _, waysToBeat := range waysToBeatEveryRace {
		result *= waysToBeat
	}
	return result
}

type Race struct {
	timeAvailable      int
	distanceToTraverse int
}
