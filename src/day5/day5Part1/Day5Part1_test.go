package day5Part1

import "testing"

func TestPrintSolution(t *testing.T) {
	PrintSolution("day5/day5.txt")
}

func TestPrintSolutionDemo(t *testing.T) {
	PrintSolution("day5/day5Part1Demo.txt")
}
