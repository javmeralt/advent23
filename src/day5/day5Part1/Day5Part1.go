package day5Part1

import (
	"advent23/src/utils/collectionUtils"
	"advent23/src/utils/fileUtils"
	"advent23/src/utils/numberUtils"
	"advent23/src/utils/stringUtils"
	"strings"
)

func PrintSolution(file string) {
	lines := fileUtils.ReadLines(file)
	seedsMaps := getSeedsMaps(lines)
	destinations := searchTheDestinationOfEverySeed(seedsMaps)
	minDestination := collectionUtils.MinIntegerInArray(destinations)
	print(minDestination)
}

func getSeedsMaps(lines []string) SeedsMaps {
	seedsMaps := SeedsMaps{}
	var nextMapString []string
	for index, line := range lines {
		if index == 0 {
			seedsMaps.seeds = processSeedSectionOfInput(line)
		} else if (len(line) == 0 || index == len(lines)-1) && len(nextMapString) > 0 {
			seedsMaps.maps = append(seedsMaps.maps, processMapSectionOfInput(nextMapString))
			nextMapString = []string{}
		} else if len(line) > 0 {
			nextMapString = append(nextMapString, line)
		}
	}
	return seedsMaps
}

func processSeedSectionOfInput(line string) []int {
	doubleColonPosition := strings.Index(line, ":")
	lineWithoutHeader := line[doubleColonPosition+1:]
	return getValuesAsIntArray(lineWithoutHeader)
}

func processMapSectionOfInput(lines []string) []MapRange {
	var newMapRange []MapRange
	for i := 1; i < len(lines); i++ {
		values := getValuesAsIntArray(lines[i])
		newMapValues := getMapRange(values)
		newMapRange = append(newMapRange, newMapValues)
	}
	return newMapRange
}

func getMapRange(values []int) MapRange {
	destination := values[0]
	source := values[1]
	mapRange := values[2]
	return MapRange{
		destination: destination,
		source:      source,
		mapRange:    mapRange,
	}
}

func getValuesAsIntArray(line string) []int {
	var valuesArray []int
	values := stringUtils.SplitTrim(line, " ")
	for _, valueAsString := range values {
		valueAsInt := numberUtils.ToInt(valueAsString)
		valuesArray = append(valuesArray, valueAsInt)
	}
	return valuesArray
}

func searchTheDestinationOfEverySeed(maps SeedsMaps) []int {
	var destinations []int
	for _, seed := range maps.seeds {
		destination := searchDestinationOfASeed(maps, seed)
		destinations = append(destinations, destination)
	}
	return destinations
}

func searchDestinationOfASeed(maps SeedsMaps, seed int) int {
	destination := seed
	for _, nextMap := range maps.maps {
		newDestination, newDestinationExists := searchDestinationInARange(destination, nextMap)
		if newDestinationExists {
			destination = newDestination
		}
	}
	return destination
}

func searchDestinationInARange(destination int, nextMapRange []MapRange) (int, bool) {
	for _, nextRange := range nextMapRange {
		destinationIsInRange := destination >= nextRange.source && destination <= nextRange.source+nextRange.mapRange
		if destinationIsInRange {
			position := destination - nextRange.source
			return nextRange.destination + position, true
		}
	}
	return destination, false
}

type SeedsMaps struct {
	seeds []int
	maps  [][]MapRange
}

type MapRange struct {
	destination int
	source      int
	mapRange    int
}
