package day5Part2

import (
	"advent23/src/utils/collectionUtils"
	"advent23/src/utils/fileUtils"
	"advent23/src/utils/numberUtils"
	"advent23/src/utils/stringUtils"
	"strings"
)

func PrintSolution(file string) {
	lines := fileUtils.ReadLines(file)
	seedsMaps := getSeedsMaps(lines)
	destinations := searchTheDestinationOfEverySeed(seedsMaps)
	minDestination := collectionUtils.MinIntegerInArray(destinations)
	print(minDestination)
}

func searchTheDestinationOfEverySeed(maps SeedsMaps) []int {
	var destinations []int
	for _, seedRange := range maps.seeds {
		for seed := seedRange.start; seed <= seedRange.end; seed++ {
			destination := searchDestinationOfASeed(maps, seed)
			destinations = append(destinations, destination)
		}
	}
	return destinations
}

func searchDestinationOfASeed(maps SeedsMaps, seed int) int {
	destination := seed
	for _, nextMap := range maps.maps {
		newDestination, newDestinationExists := searchDestinationInARange(destination, nextMap)
		if newDestinationExists {
			destination = newDestination
		}
	}
	return destination
}

func searchDestinationInARange(destination int, nextMapRange []MapRange) (int, bool) {
	for _, nextRange := range nextMapRange {
		destinationIsInRange := destination >= nextRange.source && destination <= nextRange.source+nextRange.mapRange
		if destinationIsInRange {
			position := destination - nextRange.source
			return nextRange.destination + position, true
		}
	}
	return destination, false
}

func getSeedsMaps(lines []string) SeedsMaps {
	seedsMaps := SeedsMaps{}
	var nextMapString []string
	for index, line := range lines {
		if index == 0 {
			seedsMaps.seeds = processSeedSection(line)
		} else if (len(line) == 0 || index == len(lines)-1) && len(nextMapString) > 0 {
			seedsMaps.maps = append(seedsMaps.maps, getNewMap(nextMapString))
			nextMapString = []string{}
		} else if len(line) > 0 {
			nextMapString = append(nextMapString, line)
		}
	}
	return seedsMaps
}

func getNewMap(lines []string) []MapRange {
	var newMapRange []MapRange
	for i := 1; i < len(lines); i++ {
		values := getValuesAsIntArray(lines[i])
		newMapValues := getMapRange(values)
		newMapRange = append(newMapRange, newMapValues)
	}
	return newMapRange
}

func getMapRange(values []int) MapRange {
	destination := values[0]
	source := values[1]
	mapRange := values[2]
	return MapRange{
		destination: destination,
		source:      source,
		mapRange:    mapRange,
	}
}

func processSeedSection(line string) []SeedRange {
	var seedRanges []SeedRange
	doubleColonPosition := strings.Index(line, ":")
	lineWithoutHeader := line[doubleColonPosition+1:]
	values := getValuesAsIntArray(lineWithoutHeader)
	for i := 0; i < len(values); i += 2 {
		start := values[i]
		seedRange := values[i+1]
		newSeedRange := SeedRange{
			start: start,
			end:   start + seedRange,
		}
		seedRanges = append(seedRanges, newSeedRange)
	}
	return seedRanges
}

func getValuesAsIntArray(line string) []int {
	var valuesArray []int
	values := stringUtils.SplitTrim(line, " ")
	for _, valueAsString := range values {
		valueAsInt := numberUtils.ToInt(valueAsString)
		valuesArray = append(valuesArray, valueAsInt)
	}
	return valuesArray
}

type SeedsMaps struct {
	seeds []SeedRange
	maps  [][]MapRange
}

type SeedRange struct {
	start int
	end   int
}

type MapRange struct {
	destination int
	source      int
	mapRange    int
}
