package day7

import (
	"advent23/src/utils/collectionUtils"
	"sort"
)

var (
	ranks = make(map[string]Rank)
)

func init() {
	initMapOfRanks()
}

func initMapOfRanks() {
	ranks["high"] = Rank{Name: "high", Power: 1}
	ranks["pair"] = Rank{Name: "pair", Power: 2}
	ranks["twoPair"] = Rank{Name: "twoPair", Power: 3}
	ranks["three"] = Rank{Name: "three", Power: 4}
	ranks["full"] = Rank{Name: "full", Power: 5}
	ranks["poker"] = Rank{Name: "poker", Power: 6}
	ranks["repoker"] = Rank{Name: "repoker", Power: 7}
}

func GetRankOfCard(card string) Rank {
	mapBySymbol := getMapBySymbol(card)
	countBySymbol := getCountOfSymbols(mapBySymbol)
	maxAmountOfOccurrences, secondHighAmountOfOccurrences := getFirstAndSecondMaxAmountOfOccurrences(countBySymbol)
	return getRankBasedOnAmountOfOccurrences(maxAmountOfOccurrences, secondHighAmountOfOccurrences)
}

func GetRankOfCardWithJokers(card string) Rank {
	mapBySymbol := getMapBySymbol(card)
	numberOfJokers, jokerPresent := mapBySymbol['J']
	delete(mapBySymbol, 'J')
	countBySymbol := getCountOfSymbols(mapBySymbol)
	maxAmountOfOccurrences, secondHighAmountOfOccurrences := getFirstAndSecondMaxAmountOfOccurrences(countBySymbol)
	if jokerPresent {
		maxAmountOfOccurrences += numberOfJokers
	}
	return getRankBasedOnAmountOfOccurrences(maxAmountOfOccurrences, secondHighAmountOfOccurrences)
}

func getFirstAndSecondMaxAmountOfOccurrences(countOfSymbols []int) (int, int) {
	var maxAmountOfOccurrences, secondHighAmountOfOccurrences int
	differentAmountOfSymbols := len(countOfSymbols)
	maxAmountOfOccurrences = getMaxAmountOfOccurrences(countOfSymbols, differentAmountOfSymbols)
	secondHighAmountOfOccurrences = getSecondHighestAmountOfOccurrences(countOfSymbols, differentAmountOfSymbols)
	return maxAmountOfOccurrences, secondHighAmountOfOccurrences
}

func getSecondHighestAmountOfOccurrences(countOfSymbols []int, differentAmountOfSymbols int) int {
	if differentAmountOfSymbols > 1 {
		return countOfSymbols[1]
	} else {
		return 0
	}
}

func getMaxAmountOfOccurrences(countOfSymbols []int, differentAmountOfSymbols int) int {
	thereWereOnlyJokers := differentAmountOfSymbols == 0
	if thereWereOnlyJokers {
		return 0
	} else {
		return countOfSymbols[0]
	}
}

func getRankBasedOnAmountOfOccurrences(maxAmountOfOccurrences int, secondHighAmountOfOccurrences int) Rank {
	if maxAmountOfOccurrences == 5 {
		return ranks["repoker"]
	} else if maxAmountOfOccurrences == 4 {
		return ranks["poker"]
	} else if maxAmountOfOccurrences == 3 && secondHighAmountOfOccurrences == 2 {
		return ranks["full"]
	} else if maxAmountOfOccurrences == 3 {
		return ranks["three"]
	} else if maxAmountOfOccurrences == 2 && secondHighAmountOfOccurrences == 2 {
		return ranks["twoPair"]
	} else if maxAmountOfOccurrences == 2 {
		return ranks["pair"]
	} else {
		return ranks["high"]
	}
}

func getCountOfSymbols(countBySymbols map[rune]int) []int {
	mapValues := collectionUtils.GetMapValues(countBySymbols)
	sort.Sort(sort.Reverse(sort.IntSlice(mapValues)))
	return mapValues
}

func getMapBySymbol(card string) map[rune]int {
	elements := map[rune]int{}
	runeCards := []rune(card)
	for _, runeCard := range runeCards {
		elements[runeCard]++
	}
	return elements
}

type CamelCards struct {
	Cards string
	Score int
}

type Rank struct {
	Name       string
	Power      int
	CamelCards CamelCards
}
