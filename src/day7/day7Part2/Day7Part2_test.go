package day7Part2

import "testing"

func TestPrintSolution(t *testing.T) {
	PrintSolution("day7/day7.txt")
}

func TestPrintSolutionDemo(t *testing.T) {
	PrintSolution("day7/day7Demo.txt")
}
