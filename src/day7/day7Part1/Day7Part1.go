package day7Part1

import (
	"advent23/src/day7"
	"advent23/src/utils/fileUtils"
	"advent23/src/utils/numberUtils"
	"advent23/src/utils/stringUtils"
	"sort"
)

var (
	symbols = make(map[rune]int)
)

func init() {
	initMapOfSymbols()
}

func initMapOfSymbols() {
	symbols['A'] = 13
	symbols['K'] = 12
	symbols['Q'] = 11
	symbols['J'] = 10
	symbols['T'] = 9
	symbols['9'] = 8
	symbols['8'] = 7
	symbols['7'] = 6
	symbols['6'] = 5
	symbols['5'] = 4
	symbols['4'] = 3
	symbols['3'] = 2
	symbols['2'] = 1
	symbols['1'] = 0
}
func PrintSolution(file string) {
	lines := fileUtils.ReadLines(file)
	camelCards := createCamelCardsFromInput(lines)
	ranks := GetRanks(camelCards)
	total := getTotal(ranks)
	print(total)
}

func getTotal(ranks []day7.Rank) int {
	total := 0
	for index, rank := range ranks {
		rankPosition := index + 1
		total += rankPosition * rank.CamelCards.Score
	}
	return total
}

func createCamelCardsFromInput(lines []string) []day7.CamelCards {
	var camelCardsList []day7.CamelCards
	for _, line := range lines {
		camelCards := createCamelCardFromInput(line)
		camelCardsList = append(camelCardsList, camelCards)
	}
	return camelCardsList
}

func createCamelCardFromInput(line string) day7.CamelCards {
	cardsAndScore := stringUtils.SplitTrim(line, " ")
	cards := cardsAndScore[0]
	score := numberUtils.ToInt(cardsAndScore[1])
	return day7.CamelCards{Cards: cards, Score: score}
}

func GetRanks(cards []day7.CamelCards) []day7.Rank {
	var ranksList []day7.Rank
	for _, card := range cards {
		rank := day7.GetRankOfCard(card.Cards)
		rank.CamelCards = card
		ranksList = append(ranksList, rank)
	}
	return sortRankListByRankScore(ranksList)
}

func sortRankListByRankScore(list []day7.Rank) []day7.Rank {
	sort.SliceStable(list, func(i, j int) bool {
		e1 := list[i]
		e2 := list[j]
		if e1.Power == e2.Power {
			return compareSymbolBySymbol(e1.CamelCards.Cards, e2.CamelCards.Cards)
		} else {
			return e1.Power < e2.Power
		}
	})
	return list
}

func compareSymbolBySymbol(cards1Str string, cards2Str string) bool {
	cards1 := []rune(cards1Str)
	cards2 := []rune(cards2Str)
	for i := 0; i < len(cards1); i++ {
		powerSymbol1 := symbols[cards1[i]]
		powerSymbol2 := symbols[cards2[i]]
		if powerSymbol1 < powerSymbol2 {
			return true
		} else if powerSymbol1 > powerSymbol2 {
			return false
		}
	}
	return true
}
