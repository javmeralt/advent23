package day3Part1

import "testing"

func TestPrintSolution(t *testing.T) {
	PrintSolution("day3/day3.txt")
}

func TestPrintSolutionDemo(t *testing.T) {
	PrintSolution("day3/day3Part1Demo.txt")
}
