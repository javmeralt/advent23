package day3Part1

import (
	"advent23/src/day3"
	"advent23/src/utils/fileUtils"
	"advent23/src/utils/numberUtils"
	"unicode"
)

func PrintSolution(file string) {
	lines := fileUtils.ReadLines(file)
	matrix := createMatrix(lines)
	total := searchMatrix(matrix)
	print(total)
}

func createMatrix(lines []string) [][]rune {
	matrix := make([][]rune, len(lines))
	for index, line := range lines {
		matrix[index] = []rune(line)
	}
	return matrix
}

func searchMatrix(matrix [][]rune) int {
	totalScore := 0
	for rowIndex, row := range matrix {
		rowScore := searchRow(row, matrix, rowIndex)
		totalScore += rowScore
	}
	return totalScore
}

func searchRow(row []rune, matrix [][]rune, rowIndex int) int {
	columnIndex := 0
	totalInRow := 0
	for ; columnIndex < len(row); columnIndex++ {
		nextNumber, numberFound := getNextNumber(row, columnIndex, rowIndex)
		if numberFound {
			totalInRow += getScore(matrix, nextNumber)
			columnIndex = nextNumber.EndPosition.X + 1
		} else {
			break
		}
	}
	return totalInRow
}

func getNextNumber(row []rune, columnIndex int, rowIndex int) (day3.NumberPosition, bool) {
	var start int
	numberFound := false
	for ; columnIndex < len(row); columnIndex++ {
		if unicode.IsDigit(row[columnIndex]) && !numberFound {
			start = columnIndex
			numberFound = true
		} else if !unicode.IsDigit(row[columnIndex]) && numberFound {
			break
		}
	}
	if numberFound {
		number := numberUtils.ToInt(string(row[start:columnIndex]))
		return getNumberPosition(start, columnIndex-1, number, rowIndex), numberFound
	} else {
		return day3.NumberPosition{EndPosition: day3.Position{X: columnIndex}}, numberFound
	}
}

func getScore(matrix [][]rune, number day3.NumberPosition) int {
	neighborPositions := day3.GetNeighborPositionsOfRangeOfPositions(matrix, number)
	if day3.AnyNeighborIsDifferentToNumberOrDot(matrix, neighborPositions) {
		return number.Number
	} else {
		return 0
	}
}

func getNumberPosition(start int, end int, number int, rowIndex int) day3.NumberPosition {
	return day3.NumberPosition{StartPosition: day3.Position{X: start, Y: rowIndex}, EndPosition: day3.Position{X: end, Y: rowIndex}, Number: number}
}
