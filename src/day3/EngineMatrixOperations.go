package day3

import (
	"advent23/src/utils/collectionUtils"
	"unicode"
)

var (
	possibleNeighbors = map[string]Position{
		"upLeft":    {Y: 1, X: -1},
		"up":        {Y: 1, X: 0},
		"upRight":   {Y: 1, X: 1},
		"left":      {Y: 0, X: -1},
		"right":     {Y: 0, X: 1},
		"downLeft":  {Y: -1, X: -1},
		"down":      {Y: -1, X: 0},
		"downRight": {Y: -1, X: 1},
	}
)

func GetNeighborPositionsOfRangeOfPositions(matrix [][]rune, number NumberPosition) []Position {
	currentRow := number.StartPosition.Y
	neighborPositionsMap := map[Position]bool{}
	for i := number.StartPosition.X; i <= number.EndPosition.X; i++ {
		position := getPosition(i, currentRow)
		newNeighborPositions := GetNeighborPositions(matrix, position)
		collectionUtils.PutAll(neighborPositionsMap, newNeighborPositions)
	}
	return collectionUtils.GetMapKeys(neighborPositionsMap)
}

func GetNeighborPositionsAsList(matrix [][]rune, positionToConsider Position) []Position {
	return collectionUtils.GetMapKeys(GetNeighborPositions(matrix, positionToConsider))
}

func GetNeighborPositions(matrix [][]rune, positionToConsider Position) map[Position]bool {
	maxY := len(matrix)
	maxX := len(matrix[positionToConsider.Y])
	neighborPositionsMap := map[Position]bool{}
	for _, possibleNeighborPosition := range possibleNeighbors {
		nextPosition := nextPosition(possibleNeighborPosition, positionToConsider)
		if validPosition(nextPosition, maxX, maxY) {
			neighborPositionsMap[nextPosition] = true
		}
	}
	return neighborPositionsMap
}

func getPosition(i int, row int) Position {
	return Position{X: i, Y: row}
}

func nextPosition(possibleNeighborPosition Position, positionToConsider Position) Position {
	return Position{Y: positionToConsider.Y + possibleNeighborPosition.Y, X: positionToConsider.X + possibleNeighborPosition.X}
}

func validPosition(position Position, maxX int, maxY int) bool {
	return position.Y < maxY && position.Y >= 0 && position.X < maxX && position.X >= 0
}

func AnyNeighborIsDifferentToNumberOrDot(matrix [][]rune, neighborPositions []Position) bool {
	for _, neighborPosition := range neighborPositions {
		symbol := matrix[neighborPosition.Y][neighborPosition.X]
		if !unicode.IsDigit(symbol) && symbol != '.' {
			return true
		}
	}
	return false
}

func GetMatrixContent(matrix [][]rune, position Position) rune {
	return matrix[position.Y][position.X]
}

type Position struct {
	X int
	Y int
}

type NumberPosition struct {
	StartPosition Position
	EndPosition   Position
	Number        int
}
