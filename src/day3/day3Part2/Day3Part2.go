package day3Part2

import (
	"advent23/src/day3"
	"advent23/src/utils/fileUtils"
	"advent23/src/utils/numberUtils"
	"advent23/src/utils/stringUtils"
	"unicode"
)

func PrintSolution(file string) {
	lines := fileUtils.ReadLines(file)
	matrix := createMatrix(lines)
	total := searchMatrix(matrix)
	print(total)
}

func createMatrix(lines []string) [][]rune {
	matrix := make([][]rune, len(lines))
	for index, line := range lines {
		matrix[index] = []rune(line)
	}
	return matrix
}

func searchMatrix(matrix [][]rune) int {
	totalScore := 0
	for rowIndex, row := range matrix {
		rowScore := searchRow(row, matrix, rowIndex)
		totalScore += rowScore
	}
	return totalScore
}

func searchRow(row []rune, matrix [][]rune, rowIndex int) int {
	columnIndex := 0
	totalInRow := 0
	for columnIndex < len(row) {
		asteriskPosition, asteriskFound := getNextAsterisk(row, columnIndex, rowIndex)
		if asteriskFound {
			totalInRow += getScore(matrix, asteriskPosition)
			columnIndex = asteriskPosition.X + 1
		} else {
			break
		}
	}
	return totalInRow
}

func getNextAsterisk(row []rune, columnIndex int, rowIndex int) (day3.Position, bool) {
	for ; columnIndex < len(row); columnIndex++ {
		if row[columnIndex] == '*' {
			return day3.Position{X: columnIndex, Y: rowIndex}, true
		}
	}
	return day3.Position{}, false
}

func getScore(matrix [][]rune, asteriskPosition day3.Position) int {
	neighbors := day3.GetNeighborPositionsAsList(matrix, asteriskPosition)
	alreadyTraversedPosition := map[day3.Position]bool{}
	var numbers []int
	for _, neighbor := range neighbors {
		matrixContent := day3.GetMatrixContent(matrix, neighbor)
		if unicode.IsDigit(matrixContent) {
			startOfNumber := goToStartOfNumber(matrix, neighbor)
			if !alreadyTraversedPosition[startOfNumber] {
				number := getNumber(matrix, startOfNumber)
				numbers = append(numbers, number)
				alreadyTraversedPosition[startOfNumber] = true
			}
		}
	}
	if len(numbers) == 2 {
		return numbers[0] * numbers[1]
	} else {
		return 0
	}
}

func goToStartOfNumber(matrix [][]rune, neighbor day3.Position) day3.Position {
	currentPosition := neighbor.X
	for currentPosition-1 >= 0 {
		previousPosition := currentPosition - 1
		if unicode.IsDigit(matrix[neighbor.Y][previousPosition]) {
			currentPosition--
		} else {
			break
		}
	}
	neighbor.X = currentPosition
	return neighbor
}

func getNumber(matrix [][]rune, neighbor day3.Position) int {
	start := neighbor.X
	row := matrix[neighbor.Y]
	end := neighbor.X
	for ; end < len(row); end++ {
		possibleNumber := row[end]
		if !unicode.IsDigit(possibleNumber) {
			break
		}
	}
	numberAsString := stringUtils.SliceRuneFromStartToEnd(row, start, end)
	return numberUtils.ToInt(numberAsString)
}
