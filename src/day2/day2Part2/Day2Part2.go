package day2Part2

import (
	"advent23/src/utils/fileUtils"
	"advent23/src/utils/numberUtils"
	"advent23/src/utils/stringUtils"
	"strings"
)

type NumberOfBalls struct {
	reds   int
	greens int
	blues  int
}

func PrintSolution(file string) {
	lines := fileUtils.ReadLines(file)
	total := 0
	for _, line := range lines {
		setsOfBalls := processLine(line)
		total += getScoreOfLine(setsOfBalls)
	}
	print(total)
}

func processLine(line string) []NumberOfBalls {
	headerAndBalls := strings.Split(line, ":")
	balls := headerAndBalls[1]
	numberOfBalls := getNumberOfBalls(balls)
	return numberOfBalls
}

func getNumberOfBalls(everySetOfBalls string) []NumberOfBalls {
	var numberOfBalls []NumberOfBalls
	separatedSetsOfBalls := strings.Split(everySetOfBalls, ";")
	for _, setOfBalls := range separatedSetsOfBalls {
		processedSetOfBalls := processSetOfBalls(setOfBalls)
		numberOfBalls = append(numberOfBalls, processedSetOfBalls)
	}
	return numberOfBalls
}

func processSetOfBalls(balls string) NumberOfBalls {
	splitBalls := stringUtils.SplitTrim(balls, ",")
	setOfBalls := createEmptyNumberOfBalls()
	for _, splitBall := range splitBalls {
		numberAndColor := strings.Split(splitBall, " ")
		number := numberUtils.ToInt(numberAndColor[0])
		color := numberAndColor[1]
		fillNumberOfBallsField(&setOfBalls, number, color)
	}
	return setOfBalls
}

func fillNumberOfBallsField(setOfBalls *NumberOfBalls, number int, color string) {
	switch color {
	case "green":
		setOfBalls.greens = number
	case "red":
		setOfBalls.reds = number
	case "blue":
		setOfBalls.blues = number
	}
}

func createEmptyNumberOfBalls() NumberOfBalls {
	return NumberOfBalls{
		reds:   0,
		greens: 0,
		blues:  0,
	}
}

func getScoreOfLine(setsOfBalls []NumberOfBalls) int {
	ballsNeeded := createNumberOfBallsWithOnes()
	for _, setOfBalls := range setsOfBalls {
		ballsNeeded.reds = max(setOfBalls.reds, ballsNeeded.reds)
		ballsNeeded.greens = max(setOfBalls.greens, ballsNeeded.greens)
		ballsNeeded.blues = max(setOfBalls.blues, ballsNeeded.blues)
	}
	return ballsNeeded.blues * ballsNeeded.greens * ballsNeeded.reds
}

func createNumberOfBallsWithOnes() NumberOfBalls {
	return NumberOfBalls{
		reds:   1,
		greens: 1,
		blues:  1,
	}
}
