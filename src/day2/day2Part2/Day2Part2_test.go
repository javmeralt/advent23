package day2Part2

import (
	"testing"
)

func TestPrintSolutionDemo(t *testing.T) {
	PrintSolution("day2/day2Part1Demo.txt")
}

func TestPrintSolution(t *testing.T) {
	PrintSolution("day2/day2.txt")
}
