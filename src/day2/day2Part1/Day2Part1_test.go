package day2Part1

import (
	"testing"
)

func TestPrintSolutionDemo(t *testing.T) {
	balls := NumberOfBalls{
		reds:   12,
		greens: 13,
		blues:  14,
	}
	PrintSolution("day2/day2Part1Demo.txt", balls)
}

func TestPrintSolution(t *testing.T) {
	balls := NumberOfBalls{
		reds:   12,
		greens: 13,
		blues:  14,
	}
	PrintSolution("day2/day2.txt", balls)
}
