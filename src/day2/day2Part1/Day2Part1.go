package day2Part1

import (
	"advent23/src/utils/fileUtils"
	"advent23/src/utils/numberUtils"
	"advent23/src/utils/stringUtils"
	"strconv"
	"strings"
)

type NumberOfBalls struct {
	reds   int
	greens int
	blues  int
}

func PrintSolution(file string, ballsAvailable NumberOfBalls) {
	lines := fileUtils.ReadLines(file)
	total := 0
	for _, line := range lines {
		gameNumber, setsOfBalls := processLine(line)
		total += getScoreOfLine(gameNumber, setsOfBalls, ballsAvailable)
	}
	print(total)
}

func processLine(line string) (int, []NumberOfBalls) {
	headerAndBalls := strings.Split(line, ":")
	header := headerAndBalls[0]
	balls := headerAndBalls[1]
	gameNumber := getGameNumber(header)
	numberOfBalls := getNumberOfBalls(balls)
	return gameNumber, numberOfBalls
}

func getGameNumber(header string) int {
	intValue, _ := strconv.Atoi(header[5:])
	return intValue
}

func getNumberOfBalls(everySetOfBalls string) []NumberOfBalls {
	var numberOfBalls []NumberOfBalls
	separatedSetsOfBalls := strings.Split(everySetOfBalls, ";")
	for _, setOfBalls := range separatedSetsOfBalls {
		processedSetOfBalls := processSetOfBalls(setOfBalls)
		numberOfBalls = append(numberOfBalls, processedSetOfBalls)
	}
	return numberOfBalls
}

func processSetOfBalls(balls string) NumberOfBalls {
	splitBalls := stringUtils.SplitTrim(balls, ",")
	setOfBalls := createEmptyNumberOfBalls()
	for _, splitBall := range splitBalls {
		numberAndColor := strings.Split(splitBall, " ")
		number := numberUtils.ToInt(numberAndColor[0])
		color := numberAndColor[1]
		fillNumberOfBallsField(&setOfBalls, number, color)
	}
	return setOfBalls
}

func fillNumberOfBallsField(setOfBalls *NumberOfBalls, number int, color string) {
	switch color {
	case "green":
		setOfBalls.greens = number
	case "red":
		setOfBalls.reds = number
	case "blue":
		setOfBalls.blues = number
	}
}

func createEmptyNumberOfBalls() NumberOfBalls {
	return NumberOfBalls{
		reds:   0,
		greens: 0,
		blues:  0,
	}
}

func getScoreOfLine(number int, setsOfBalls []NumberOfBalls, available NumberOfBalls) int {
	for _, setOfBalls := range setsOfBalls {
		if notEnoughGreen(setOfBalls, available.greens) ||
			notEnoughRed(setOfBalls, available.reds) ||
			notEnoughBlue(setOfBalls, available.blues) {
			return 0
		}
	}
	return number
}

func notEnoughGreen(setOfBalls NumberOfBalls, greens int) bool {
	return setOfBalls.greens > greens
}

func notEnoughRed(setOfBalls NumberOfBalls, reds int) bool {
	return setOfBalls.reds > reds
}

func notEnoughBlue(setOfBalls NumberOfBalls, blues int) bool {
	return setOfBalls.blues > blues
}
