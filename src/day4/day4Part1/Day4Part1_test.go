package day4Part1

import "testing"

func TestPrintSolution(t *testing.T) {
	PrintSolution("day4/day4.txt")
}

func TestPrintSolutionDemo(t *testing.T) {
	PrintSolution("day4/day4Part1Demo.txt")
}
