package day4Part1

import (
	"advent23/src/utils/fileUtils"
	"advent23/src/utils/numberUtils"
	"advent23/src/utils/stringUtils"
	"strings"
)

func PrintSolution(file string) {
	lines := fileUtils.ReadLines(file)
	score := 0
	for _, line := range lines {
		card := getCardFromLine(line)
		score += getScoreOfCard(card)
	}
	print(score)
}

func getCardFromLine(line string) Card {
	doubleColonPosition := strings.Index(line, ":")
	slashPosition := strings.Index(line, "|")
	winningNumbersAsString := line[doubleColonPosition+1 : slashPosition]
	numbersYouHaveAsString := line[slashPosition+1:]
	return Card{winningNumbers: getCardValues(winningNumbersAsString), numbersYouHave: getCardValues(numbersYouHaveAsString)}
}

func getCardValues(cardAsString string) []string {
	return stringUtils.SplitTrim(cardAsString, " ")
}

func getScoreOfCard(card Card) int {
	winningNumbersSet := getWinningNumbersAsSet(card.winningNumbers)
	hits := -1
	for _, numberYouHave := range card.numbersYouHave {
		if winningNumbersSet[numberYouHave] {
			hits++
		}
	}
	if hits >= 0 {
		return numberUtils.IntPow(2, hits)
	} else {
		return 0
	}
}

func getWinningNumbersAsSet(numbers []string) map[string]bool {
	winningNumbersSet := map[string]bool{}
	for _, number := range numbers {
		winningNumbersSet[number] = true
	}
	return winningNumbersSet
}

type Card struct {
	winningNumbers []string
	numbersYouHave []string
}
