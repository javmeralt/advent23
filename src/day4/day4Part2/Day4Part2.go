package day4Part1

import (
	"advent23/src/utils/fileUtils"
	"advent23/src/utils/numberUtils"
	"advent23/src/utils/stringUtils"
	"strings"
)

func PrintSolution(file string) {
	lines := fileUtils.ReadLines(file)
	numberOfCards := len(lines)
	cards, cardsMap := getEveryCardWithTheAmountOfCardsItWins(lines, numberOfCards)
	numberOfCardsWon := processCards(cards, cardsMap, numberOfCards)
	print(numberOfCardsWon)
}

func processCards(cardsToProcess []Card, cardsMap map[int]Card, numberOfCards int) int {
	total := 0
	for _, card := range cardsToProcess {
		nextCardsToProcess := getNextCardsToProcess(cardsMap, card)
		total += processCards(nextCardsToProcess, cardsMap, numberOfCards) + 1
	}
	return total
}

func getNextCardsToProcess(cardsMap map[int]Card, card Card) []Card {
	start := card.id + 1
	end := start + card.score
	var nextCards []Card
	for ; start < end; start++ {
		nextCards = append(nextCards, cardsMap[start])
	}
	return nextCards
}

func getEveryCardWithTheAmountOfCardsItWins(lines []string, numberOfCards int) ([]Card, map[int]Card) {
	cardsMap := map[int]Card{}
	var cards []Card
	for _, line := range lines {
		card := getCardFromLineWithTheAmountOfCardsItWins(line, numberOfCards)
		cardsMap[card.id] = card
		cards = append(cards, card)
	}
	return cards, cardsMap
}

func getCardFromLineWithTheAmountOfCardsItWins(line string, numberOfCards int) Card {
	card := getCardWithoutTheAmountOfCardsItWins(line)
	score := getNumberOfCardsThatTheCardWins(card, numberOfCards)
	card.score = score
	return card
}

func getCardWithoutTheAmountOfCardsItWins(line string) Card {
	doubleColonPosition := strings.Index(line, ":")
	slashPosition := strings.Index(line, "|")
	winningNumbersAsString := line[doubleColonPosition+1 : slashPosition]
	numbersYouHaveAsString := line[slashPosition+1:]
	cardIdAsString := line[4:doubleColonPosition]
	cardId := numberUtils.ToInt(stringUtils.RemoveRedundantSpaces(cardIdAsString))
	card := Card{id: cardId, winningNumbers: getCardValues(winningNumbersAsString), numbersYouHave: getCardValues(numbersYouHaveAsString)}
	return card
}

func getCardValues(cardAsString string) []string {
	return stringUtils.SplitTrim(cardAsString, " ")
}

func getNumberOfCardsThatTheCardWins(card Card, numberOfCards int) int {
	winningNumbersSet := getWinningNumbersAsSet(card.winningNumbers)
	possibleNumberOfCardsWon := numberOfCards - card.id
	hits := 0
	for _, numberYouHave := range card.numbersYouHave {
		if winningNumbersSet[numberYouHave] {
			hits++
		}
	}
	return min(hits, possibleNumberOfCardsWon)
}

func getWinningNumbersAsSet(numbers []string) map[string]bool {
	winningNumbersSet := map[string]bool{}
	for _, number := range numbers {
		winningNumbersSet[number] = true
	}
	return winningNumbersSet
}

type Card struct {
	id             int
	winningNumbers []string
	numbersYouHave []string
	score          int
}
